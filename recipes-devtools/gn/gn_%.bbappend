# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

# OpenHarmony build system relies on the obsolete set_sources_assignment_filter
# GN function which was recently dropped, therefore downgrading GN to revision
# compatible with OpenHarmony

SRCREV:oniro-openharmony-linux = "5da62d5e9d0f10cb8ece7c30563a6a214c78b68d"
